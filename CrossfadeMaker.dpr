program CrossfadeMaker;

uses
  Vcl.Forms,
  Unit1 in 'Unit1.pas' {FormXFade},
  Unit2 in 'Unit2.pas' {FormRandom},
  Unit3 in 'Unit3.pas' {ProgressDlg},
  Version in 'Version.pas',
  encoder in 'encoder.pas',
  HTMLBuilder in 'HTMLBuilder.pas',
  common in 'common.pas';

{$R *.res}

begin
  Application.Initialize;
  Application.MainFormOnTaskbar := True;
  Application.Title := 'クロスフェードメーカー';
  Application.CreateForm(TFormXFade, FormXFade);
  Application.CreateForm(TFormRandom, FormRandom);
  Application.Run;
end.
