unit HTMLBuilder;

interface

uses
  Windows, Sysutils, Classes, Common;

type
  THTMLBuilder = class
  private
      FBaseDir, FBuildDir : String;
      FHTML : TStringList;
      FRunMode : TCrossFadeMode;
      Fc : Integer;
  public
      property BuildDirectory:String read FBuildDir;
      constructor Create(BaseDir:String; BuildDir:String; Mode:TCrossFadeMode);
      destructor Destroy;
      procedure Write(Title:String; SoundFileName:String; Vars:String = '');
      procedure Publish(HTMLFileName:String);
  end;

implementation

constructor THTMLBuilder.Create(BaseDir:String; BuildDir:String; Mode:TCrossFadeMode);
begin
      FBaseDir := IncludeTrailingPathDelimiter(BaseDir);
      FBuildDir := IncludeTrailingPathDelimiter(BuildDir);
      FRunMode := Mode;
      FHTML := TStringList.Create;
      FHTML.LoadFromFile(FBaseDir+'header.html');
      Fc := 1;
end;

procedure THTMLBuilder.Write(Title:String; SoundFileName:String; Vars:String = '');
var
      Template : TStringList;
      s,ident,rfname : String;
      Varlist : TStringList;
      i : Integer;
begin
      Template := TStringList.Create;
      Varlist := TStringList.Create;
      try
            //Template.LoadFromFile(FBaseDir+'header.html');
            //s := Template.Text;
            s := '';

            Varlist.Text := Vars;

            if FRunMode = cfmSingle then Template.LoadFromFile(FBaseDir+'single.html')
            else if FRunMode = cfmMixed then Template.LoadFromFile(FBaseDir+'mixed.html');
            s := s + Template.Text;

            //Template.LoadFromFile(FBaseDir+'footer.html');
            //s := s + Template.Text;

            ident := Format('snd%.02d',[Fc]);
            Inc(Fc);

            rfname := ExtractRelativePath(FBuildDir,SoundFileName).Replace('\','/');

            s := StringReplace(s,'<!--IDENT-->',ident,[rfReplaceAll]);
            s := StringReplace(s,'<!--FILE-->',rfname,[rfReplaceAll]);
            s := StringReplace(s,'<!--TITLE-->',Title,[rfReplaceAll]);
            //s := StringReplace(s,'<!--JSON-->',Title,[rfReplaceAll]);

            for i := 0 to Varlist.Count-1 do
            begin
                  s := StringReplace(s,'<!--'+Varlist.Names[i]+'-->',Varlist.ValueFromIndex[i],[rfReplaceAll]);
            end;

            FHTML.Add(s);

      finally
            Template.Free;
            Varlist.Free;
      end;
end;

destructor THTMLBuilder.Destroy;
begin
      FHTML.Free;
end;

procedure THTMLBuilder.Publish(HTMLFileName:String);
var
      Template : TStringList;
begin
      Template := TStringList.Create;
      try
            Template.LoadFromFile(FBaseDir+'footer.html');
            FHTML.AddStrings(Template);
      finally
            Template.Free;
      end;

      FHTML.SaveToFile(FBuildDir+HTMLFileName,TEncoding.UTF8);
      CopyFile(PChar(FBaseDir+'jwplayer.flash.swf'),PChar(FBuildDir+'jwplayer.flash.swf'),True);
      CopyFile(PChar(FBaseDir+'jwplayer.html5.js'),PChar(FBuildDir+'jwplayer.html5.js'),True);
      CopyFile(PChar(FBaseDir+'jwplayer.js'),PChar(FBuildDir+'jwplayer.js'),True);
      CopyFile(PChar(FBaseDir+'jquery-2.0.3.min.js'),PChar(FBuildDir+'jquery-2.0.3.min.js'),True);
      CopyFile(PChar(FBaseDir+'playing.png'),PChar(FBuildDir+'playing.png'),True);
      CopyFile(PChar(FBaseDir+'soundsheet.css'),PChar(FBuildDir+'soundsheet.css'),True);
end;

end.
