﻿unit Unit2;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AdvGlowButton, Vcl.StdCtrls, Vcl.Mask,
  AdvSpin, StrUtils, ACS_Classes, ACS_Wave, ACS_smpeg, ACS_AAC, ACS_WinMedia, Math, GDIPHTMLItem,
  DateUtils;

type
  TFormRandom = class(TForm)
    AdvGlowButton1: TAdvGlowButton;
    WMIn1: TWMIn;
    MP4In1: TMP4In;
    AACIn1: TAACIn;
    MP3In1: TMP3In;
    WaveIn1: TWaveIn;
    Label1: TLabel;
    GroupBox1: TGroupBox;
    RadioParam1: TRadioButton;
    SpinParam1: TAdvSpinEdit;
    RadioParam2: TRadioButton;
    SpinParam2: TAdvSpinEdit;
    procedure AdvGlowButton1Click(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
  end;

var
  FormRandom: TFormRandom;

implementation

{$R *.dfm}

uses Unit1;

procedure TFormRandom.AdvGlowButton1Click(Sender: TObject);
const
      SPACER_PERCENT = 10{%};
var
      fpath : String;
      data : TSetData;
      i : Integer;
      AudioIn : TAuFileIn;

      TotalTime,CutTime,SpTime : Double;
      p_in,p_out : Double;

      p_in_s,p_out_s : String;
begin
      with FormXFade do
      begin
            for i := 0 to AdvPolyList1.ItemCount-1 do
            begin
                  Randomize;

                  fpath := AdvPolyList1.Items[i].Hint;
                  data := Pointer(AdvPolyList1.Items[i].Tag);

                  AudioIn := nil;

                  case AnsiIndexText(ExtractFileExt(fpath),['.wav','.mp3','.wma','.aac','.m4a','.mp4']) of
                        0: AudioIn := WaveIn1;
                        1: AudioIn := MP3In1;
                        2: AudioIn := WMIn1;
                        3,4,5 : AudioIn := AACIn1;
                  end;

                  if Assigned(AudioIn) then
                  begin
                        AudioIn.FileName := fpath;

                        AudioIn.Init;
                        try
                              TotalTime := AudioIn.TotalSamples / AudioIn.SampleRate;

                              // constant & variable
                              CutTime := -1;
                              if RadioParam1.Checked then CutTime := SpinParam1.FloatValue;
                              if RadioParam2.Checked then CutTime := TotalTime * SpinParam2.FloatValue/100;

                              if CutTime > TotalTime then CutTime := TotalTime;

                              // spacing buffer
                              SpTime := TotalTime * SPACER_PERCENT / 100;

                              p_in := RandomRange(Round(0+SpTime),Trunc(TotalTime-CutTime-SpTime));
                              p_out := p_in + CutTime;
                        finally
                              AudioIn.Flush;
                        end;

                        data.StartTime := p_in;
                        data.EndTime := p_out;

                        p_in_s := Format('%s.%.03d',[TimeToStr(UnixToDateTime(Trunc(Data.StartTime))),Trunc(Frac(Data.StartTime)*1000)]);
                        p_out_s := Format('%s.%.03d',[TimeToStr(UnixToDateTime(Trunc(Data.EndTime))),Trunc(Frac(Data.EndTime)*1000)]);


                        THTMLItem(AdvPolyList1.Items[i]).Caption := '<b>'+ChangeFileExt(ExtractFileName(fpath),'') + '</b><br>'+
                        '<font color="#999999"><font size="2">'+p_in_s+'～'+p_out_s+'</font></font>';
                  end;
            end;
      end;
end;

end.
