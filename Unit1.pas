﻿unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, AdvGlowButton, Vcl.ExtCtrls,
  GDIPCustomItem, GDIPTextItem, GDIPImageTextItem, GDIPHTMLItem,
  CustomItemsContainer, AdvPolyList, Vcl.StdCtrls, Vcl.OleCtrls, WMPLib_TLB,
  ACS_Converters, ACS_Classes, ACS_Misc, ACS_WinMedia, ACS_smpeg, ACS_Wave,
  Vcl.Mask, AdvSpin, ActiveX, KDrop, AdvSmoothLedLabel, DateUtils, AdvPanel,
  KThdCmp, ACS_AudioMix, Math, ACS_Streams, ACS_AAC, StrUtils, IniFiles,
  IdHash, IdhashSHA, AdvGDIPicture, CurvyControls, ShellAPI, Version, AdvMenus,
  AdvMenuStylers, Vcl.Menus, FolderDialog, encoder, HTMLBuilder, Common,
  HTMLPopup;

type
  TSetData = class
  private
      FStartTime : Double;
      FEndTime : Double;
  public
      constructor Create;
      property StartTime:Double read FStartTime write FStartTime;
      property EndTime:Double read FEndTime write FEndTime;
  end;

  TFormXFade = class(TForm)
    Label1: TLabel;
    AdvPolyList1: TAdvPolyList;
    AdvGlowButton1: TAdvGlowButton;
    BtnOpenFiles: TAdvGlowButton;
    BtnDelete: TAdvGlowButton;
    BtnGoBuild: TAdvGlowButton;
    AdvSpinEdit1: TAdvSpinEdit;
    ComboBox1: TComboBox;
    WaveIn1: TWaveIn;
    MP3In1: TMP3In;
    AudioConverter1: TAudioConverter;
    FastResampler1: TFastResampler;
    OpenDialog1: TOpenDialog;
    KAddDrop1: TKAddDrop;
    Timer1: TTimer;
    KThdComp1: TKThdComp;
    SaveDialog1: TSaveDialog;
    AudioProcessor1: TAudioProcessor;
    KThdComp2: TKThdComp;
    StreamOut1: TStreamOut;
    AACIn1: TAACIn;
    WMIn1: TWMIn;
    MP4In1: TMP4In;
    BtnSaveProject: TAdvGlowButton;
    SaveDialog2: TSaveDialog;
    BtnLoadProject: TAdvGlowButton;
    OpenDialog2: TOpenDialog;
    GroupBox1: TGroupBox;
    Label5: TLabel;
    Label6: TLabel;
    WindowsMediaPlayer1: TWindowsMediaPlayer;
    BtnTimeFrom: TAdvGlowButton;
    BtnTimeTo: TAdvGlowButton;
    BtnTestPlay: TAdvGlowButton;
    AdvPanel1: TAdvPanel;
    AdvSmoothLedLabel1: TAdvSmoothLedLabel;
    Label7: TLabel;
    Bevel1: TBevel;
    GroupBox2: TGroupBox;
    AdvGDIPPicture1: TAdvGDIPPicture;
    CurvyPanel1: TCurvyPanel;
    Label8: TLabel;
    AdvGDIPPicture2: TAdvGDIPPicture;
    Bevel2: TBevel;
    LinkLabel1: TLinkLabel;
    Label10: TLabel;
    AdvSpinEdit2: TAdvSpinEdit;
    AdvSpinEdit3: TAdvSpinEdit;
    Label12: TLabel;
    Panel1: TPanel;
    Panel2: TPanel;
    Panel3: TPanel;
    BtnHelp: TAdvGlowButton;
    AdvPopupMenu1: TAdvPopupMenu;
    MenuMakeCross: TMenuItem;
    MenuMakeSingle: TMenuItem;
    AdvMenuOfficeStyler1: TAdvMenuOfficeStyler;
    FolderDialog1: TFolderDialog;
    N1: TMenuItem;
    CheckWriteHTML: TMenuItem;
    CheckShiori: TMenuItem;
    CheckWriteJson: TMenuItem;
    WaveIn2: TWaveIn;
    WMAOut1: TWMAOut;
    N2: TMenuItem;
    MenuCompress: TMenuItem;
    MenuCompressLevel: TMenuItem;
    RadioItemQLOW: TMenuItem;
    RadioItemQMID: TMenuItem;
    RadioItemQHIGH: TMenuItem;
    RadioItemNoCompress: TMenuItem;
    HTMLPopup1: THTMLPopup;
    procedure BtnOpenFilesClick(Sender: TObject);
    procedure AdvPolyList1ItemSelect(Sender: TObject; Item: TCustomItem;
      var Allow: Boolean);
    procedure KAddDrop1DropFiles(Sender: TObject; Target: TWinControl;
      DropFiles: TStrings);
    procedure Timer1Timer(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure BtnTimeFromClick(Sender: TObject);
    procedure BtnTimeToClick(Sender: TObject);
    procedure BtnTestPlayClick(Sender: TObject);
    procedure KThdComp1Execute(Sender: TObject);
    procedure KThdComp1Terminate(Sender: TObject);
    procedure BtnGoBuildClick(Sender: TObject);
    procedure KThdComp2Execute(Sender: TObject);
    procedure AudioProcessorGetDataCommon(Sender: TComponent; var Buffer: Pointer;
      var Bytes: Cardinal);
    procedure AdvGlowButton1Click(Sender: TObject);
    procedure BtnSaveProjectClick(Sender: TObject);
    procedure BtnLoadProjectClick(Sender: TObject);
    procedure KThdComp2Terminate(Sender: TObject);
    procedure LinkLabel1LinkClick(Sender: TObject; const Link: string;
      LinkType: TSysLinkType);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
    procedure BtnDeleteClick(Sender: TObject);
    procedure TimeCodeLabelMouseEnter(Sender: TObject);
    procedure TimeCodeLabelMouseLeave(Sender: TObject);
    procedure TimeCodeClick(Sender: TObject);
    procedure BtnHelpClick(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure RadioItemClick(Sender: TObject);
    procedure HintShowPopup(Sender: TObject);
    procedure HintClosePopup(Sender: TObject);
  private
    { Private 宣言 }
    function OpenFile(path:String):THTMLItem;
    procedure OpenFiles(Files:TStrings);
    procedure LoadProject(FileName:String);
    procedure CalcCrossFadeConstant(t_sec:Double);
    procedure EnableMainControls(bEnable:Boolean);
    procedure LoadCompressors();
    procedure LoadSettings;
    procedure SaveSettings;
  public
    { Public 宣言 }
    CROSSFADE_TIME : Double;
    CROSSFADE_BYTES : Integer;
    CROSSFADE_MODES : TCrossFadeMode;
  end;

var
  FormXFade: TFormXFade;

implementation

{$R *.dfm}

uses Unit2, Unit3;

constructor TSetData.Create;
begin
      inherited;

      Self.FStartTime := -1;
      Self.FEndTime := -1;
end;

procedure TFormXFade.LoadSettings;
var
      Ini : TMemIniFile;
      Section : String;
      Path : String;
      v,i : Integer;
      sv : String;
begin
      Path := ExtractFileDir(Application.ExeName)+'\config.ini';

      if FileExists(Path) then
      begin
            ini := TMemIniFile.Create(Path,TEncoding.Unicode);
            try
                  Section := 'General';
                  AdvSpinEdit1.FloatValue := ini.ReadFloat(Section,'CrossDuration',10.0);
                  ComboBox1.ItemIndex := ini.ReadInteger(Section,'EnvelopeType',0);
                  CheckShiori.Checked := ini.ReadBool(Section,'WriteDetailText',False);
                  AdvSpinEdit2.FloatValue := ini.ReadFloat(Section,'BlankDurationHead',1.0);
                  AdvSpinEdit3.FloatValue := ini.ReadFloat(Section,'BlankDurationTail',1.0);
                  v := ini.ReadInteger(Section,'PlayerVolume',-1);
                  if v >= 0 then WindowsMediaPlayer1.settings.volume := v;

                  // 2013.9.27 (ver1.03) added
                  MenuMakeCross.Checked := ini.ReadBool(Section,'CrossTypeA',True);
                  MenuMakeSingle.Checked := ini.ReadBool(Section,'CrossTypeB',False);
                  CheckWriteJson.Checked := ini.ReadBool(Section,'WriteJsonFile',False);
                  CheckWriteHTML.Checked := ini.ReadBool(Section,'ExportHTMLs',False);

                  sv := ini.ReadString(Section, 'Compression', '');
                  MenuCompress.Items[0].Checked := True;
                  for i := 0 to MenuCompress.Count-1 do
                  begin
                        if (MenuCompress.Items[i].Caption = sv) then
                        begin
                              MenuCompress.Items[i].Checked := True;
                              break;
                        end;
                  end;

                  v := ini.ReadInteger(Section, 'CompressionLevel', 5);
                  for i := 0 to MenuCompressLevel.Count-1 do
                  begin
                        if (MenuCompressLevel.Items[i].Tag = v) then
                        begin
                              MenuCompressLevel.Items[i].Checked := True;
                              break;
                        end;
                  end;

            finally
                  ini.Free;
            end;
      end;
end;

procedure TFormXFade.RadioItemClick(Sender: TObject);
begin
      (Sender as TMenuitem).Checked := True;
end;

procedure TFormXFade.SaveSettings;
var
      Ini : TMemIniFile;
      Section : String;
      i : Integer;
begin
      ini := TMemIniFile.Create(ExtractFileDir(Application.ExeName)+'\config.ini',TEncoding.Unicode);
      try
            Section := 'General';
            ini.WriteFloat(Section,'CrossDuration',AdvSpinEdit1.FloatValue);
            ini.WriteInteger(Section,'EnvelopeType',ComboBox1.ItemIndex);
            ini.WriteBool(Section,'WriteDetailText',CheckShiori.Checked);

            ini.WriteFloat(Section,'BlankDurationHead',AdvSpinEdit2.FloatValue);
            ini.WriteFloat(Section,'BlankDurationTail',AdvSpinEdit3.FloatValue);
            ini.WriteInteger(Section,'PlayerVolume',WindowsMediaPlayer1.settings.volume);

            // 2013.9.27 (ver1.03) added
            ini.WriteBool(Section,'CrossTypeA',MenuMakeCross.Checked);
            ini.WriteBool(Section,'CrossTypeB',MenuMakeSingle.Checked);
            ini.WriteBool(Section,'WriteJsonFile',CheckWriteJson.Checked);
            ini.WriteBool(Section,'ExportHTMLs',CheckWriteHTML.Checked);
            for i := 0 to MenuCompress.Count-1 do
            begin
                  if (MenuCompress.Items[i].Checked) then
                  begin
                        ini.WriteString(Section, 'Compression', MenuCompress.Items[i].Caption);
                        break;
                  end;
            end;
            for i := 0 to MenuCompressLevel.Count-1 do
            begin
                  if (MenuCompressLevel.Items[i].Checked) then
                  begin
                        ini.WriteInteger(Section, 'CompressionLevel', MenuCompressLevel.Items[i].Tag);
                        break;
                  end;
            end;


            ini.UpdateFile;
      finally
            ini.Free;
      end;
end;

function TFormXFade.OpenFile(path:String):THTMLItem;
var
      Item : THTMLItem;
      Data : TSetData;
begin
      Result := nil;

      if FileExists(path) then
      begin
            Data := TSetData.Create;

            Item := (AdvPolyList1.AddItem(THTMLItem) as THTMLItem);
            Item.CaptionTop := 9;
            Item.CaptionLeft := 2;
            Item.Height := 45;
            Item.Caption := '<b>'+ChangeFileExt(ExtractFileName(path),'') + '</b><br>'+'<font color="#999999"><font size="2">Undefine</font></font>';
            Item.Hint := path;
            Item.Tag := NativeInt(Data);

            Result := Item;
      end;
end;

procedure TFormXFade.OpenFiles(Files:TStrings);
var
      i : Integer;
begin
      for i := 0 to Files.Count-1 do
      begin
            OpenFile(Files[i]);
      end;
end;

procedure TFormXFade.EnableMainControls(bEnable:Boolean);
begin
      BtnTimeFrom.Enabled := bEnable;
      BtnTimeTo.Enabled := bEnable;
      BtnTestPlay.Enabled := bEnable;

      if bEnable = False then
      begin
            WindowsMediaPlayer1.controls.stop;
            WindowsMediaPlayer1.URL := '';
            WindowsMediaPlayer1.currentPlaylist.clear;

            Label8.Caption := '';
            Label5.Caption := '0:00:00.000';
            Label6.Caption := '0:00:00.000';
            AdvSmoothLedLabel1.Caption.TimeValue := 0;
      end;
end;

procedure TFormXFade.Timer1Timer(Sender: TObject);
begin
      // MediaOpen = 13
      if (WindowsMediaPlayer1.openState = 13) then
      begin
            AdvSmoothLedLabel1.Caption.TimeValue := UnixToDateTime(Round(WindowsMediaPlayer1.controls.currentPosition));
      end;
end;

procedure TFormXFade.AdvGlowButton1Click(Sender: TObject);
begin
      FormRandom.showmodal;
end;

procedure TFormXFade.BtnDeleteClick(Sender: TObject);
var
      i : Integer;
begin
      for i := AdvPolyList1.ItemCount-1 downto 0 do
      begin
            if AdvPolyList1.Items[i].State = isSelected then AdvPolyList1.RemoveItem(i);
      end;

      EnableMainControls(False);
end;

procedure TFormXFade.BtnGoBuildClick(Sender: TObject);
var
      FN : PByte;
      i : Integer;
      errlog : TStringList;
begin
      CROSSFADE_MODES := cfmMixed;
      if (FormXFade.MenuMakeSingle.Checked) then CROSSFADE_MODES := cfmSingle;

      if (CROSSFADE_MODES = cfmMixed)and(AdvPolyList1.ItemCount <= 0) then
      begin
            MessageBox(Handle,'1つ以上のトラックが必要です。','エラー',MB_OK or MB_ICONERROR);
            exit;
      end;

      // error checks
      errlog := TStringList.Create;
      try
            for i := 0 to AdvPolyList1.ItemCount-1 do
            begin
                  with TSetData(AdvPolyList1.Items[i].Tag) do
                  begin
                        if (StartTime < 0)and(EndTime < 0) then
                        begin
                              errlog.Add(Format('%d番目のトラックの時間設定が未定義です',[i+1]));
                        end else
                        begin
                              if (StartTime < 0) then
                              begin
                                    errlog.Add(Format('%d番目のトラックの開始時間が未設定です',[i+1]));
                              end else if (EndTime < 0) then
                              begin
                                    errlog.Add(Format('%d番目のトラックの終了時間が未設定です',[i+1]));
                              end else
                              begin
                                    if StartTime > EndTime then
                                    begin
                                          errlog.Add(Format('%d番目のトラックの時間設定が不正です',[i+1]));
                                    end;
                              end;
                        end;

                  end;
            end;

            if errlog.Count > 0 then
            begin
                  MessageBox(
                        Handle,
                        PChar('◇問題が見つかりました。修正して再試行してください。'+#13#10+#13#10+errlog.Text),
                        'エラー',
                        MB_ICONERROR or MB_OK
                  );

                  exit;
            end;
      finally
            errlog.Free;
      end;

      // Single mode
      if CROSSFADE_MODES = cfmSingle then
      begin
            if FolderDialog1.Execute then
            begin
                  FN := AllocMem(Length(FolderDialog1.Directory)*2+10);
                  ZeroMemory(FN,Length(FolderDialog1.Directory)*2+10);
                  CopyMemory(FN,PChar(FolderDialog1.Directory),Length(FolderDialog1.Directory)*2);

                  ProgressDlg := TProgressDlg.Create(Self);
                  KThdComp2.Tag := NativeInt(FN);
                  KThdComp2.Start;

                  ProgressDlg.showmodal;
            end;

      // Mixed mode
      end else
      begin
            if SaveDialog1.Execute then
            begin
                  FN := AllocMem(Length(SaveDialog1.FileName)*2+10);
                  ZeroMemory(FN,Length(SaveDialog1.FileName)*2+10);
                  CopyMemory(FN,PChar(SaveDialog1.FileName),Length(SaveDialog1.FileName)*2);

                  ProgressDlg := TProgressDlg.Create(Self);
                  KThdComp2.Tag := NativeInt(FN);
                  KThdComp2.Start;

                  ProgressDlg.showmodal;
            end;
      end;
end;

procedure TFormXFade.BtnHelpClick(Sender: TObject);
begin
      if MessageBox(Handle,'オンラインマニュアルを開きますか？','確認',MB_ICONINFORMATION or MB_YESNO) = ID_YES then
      begin
            ShellExecute(Handle,'open',HELP_URL,nil,nil,SW_SHOW);
      end;
end;

procedure TFormXFade.BtnTimeFromClick(Sender: TObject);
var
      Data : TSetData;
      Item : THTMLItem;
begin
      Item := THTMLItem(WindowsMediaPlayer1.Tag);

      Data := TSetData(Item.Tag);

      if (Data.EndTime > -1)and(WindowsMediaPlayer1.controls.currentPosition > Data.EndTime) then
      begin
            raise Exception.Create('終了位置より後に設定することはできません');
      end;

      Data.StartTime := WindowsMediaPlayer1.controls.currentPosition;

      if Data.StartTime > Data.EndTime then
      begin
            Label7.Caption := '??? sec.';
      end else
      begin
            Label7.Caption := Format('%.01f sec.',[Data.EndTime - Data.StartTime]);
      end;

      Label5.Caption := Format('%s.%.03d',[TimeToStr(UnixToDateTime(Trunc(Data.StartTime))),Trunc(Frac(Data.StartTime)*1000)]);

      Item.Caption := '<b>'+ChangeFileExt(ExtractFileName(Item.Hint),'') + '</b><br>'+
      '<font color="#999999"><font size="2">'+Label5.Caption+'～'+Label6.Caption+'</font></font>';

end;

procedure TFormXFade.BtnTimeToClick(Sender: TObject);
var
      Data : TSetData;
      Item : THTMLItem;
begin
      Item := THTMLItem(WindowsMediaPlayer1.Tag);

      Data := TSetData(Item.Tag);

      if (Data.StartTime > -1)and(WindowsMediaPlayer1.controls.currentPosition < Data.StartTime) then
      begin
            raise Exception.Create('開始位置より前に設定することはできません');
      end;

      Data.EndTime := WindowsMediaPlayer1.controls.currentPosition;

      if Data.StartTime > Data.EndTime then
      begin
            Label7.Caption := '??? sec.';
      end else
      begin
            Label7.Caption := Format('%.01f sec.',[Data.EndTime - Data.StartTime]);
      end;

      Label6.Caption := Format('%s.%.03d',[TimeToStr(UnixToDateTime(Trunc(Data.EndTime))),Trunc(Frac(Data.EndTime)*1000)]);

      Item.Caption := '<b>'+ChangeFileExt(ExtractFileName(Item.Hint),'') + '</b><br>'+
      '<font color="#999999"><font size="2">'+Label5.Caption+'～'+Label6.Caption+'</font></font>';
end;

procedure TFormXFade.Button1Click(Sender: TObject);
var
      Enc : TPushEncoder;
begin

      Enc := TPushEncoder.Create('.\addon','encoder.inf');
      try
            Enc.Add('H:\Music\Emerald Green\Emerald Green - コピー.wav','H:\Music\Emerald Green\tmp.m4a','qaac.exe','LO');
            while (Enc.QueueCount > 0) do
            begin
                  Application.ProcessMessages;
                  CheckSynchronize(100);
            end;

            ShowMessage('Complete.');
      finally
            Enc.Terminate;
      end;
end;

procedure TFormXFade.BtnTestPlayClick(Sender: TObject);
var
      Data : TSetData;
begin
      Data := TSetData(THTMLItem(WindowsMediaPlayer1.Tag).Tag);

      WindowsMediaPlayer1.controls.stop;

      WindowsMediaPlayer1.uiMode := 'mini';
      KthdComp1.Tag := Integer(@Data.FEndTime);
      BtnTestPlay.Enabled := False;
      KthdComp1.Start;

      WindowsMediaPlayer1.controls.play;
      WindowsMediaPlayer1.controls.currentPosition := Data.StartTime;

end;

procedure TFormXFade.AdvPolyList1ItemSelect(Sender: TObject; Item: TCustomItem;
  var Allow: Boolean);
var
      st,ed : Double;
begin
      EnableMainControls(True);

      WindowsMediaPlayer1.URL := Item.Hint;
      //WindowsMediaPlayer1.controls.play;
      WindowsMediaPlayer1.Tag := NativeInt(Item);

      // Viewing Init
      Label5.Caption := '0:00:00.000';
      Label6.Caption := '0:00:00.000';
      AdvSmoothLedLabel1.Caption.TimeValue := 0;

      st := TSetData(Item.Tag).StartTime;
      ed := TSetData(Item.Tag).EndTime;

      if st > -1 then Label5.Caption := Format('%s.%.03d',[TimeToStr(UnixToDateTime(Trunc(st))),Trunc(Frac(st)*1000)]);
      if ed > -1 then Label6.Caption := Format('%s.%.03d',[TimeToStr(UnixToDateTime(Trunc(ed))),Trunc(Frac(ed)*1000)]);

      Label5.Tag := NativeInt(PDouble(@TSetData(Item.Tag).FStartTime));
      Label6.Tag := NativeInt(PDouble(@TSetData(Item.Tag).FEndTime));

      Label7.Caption := Format('%.01f sec.',[ed-st]);
      Label8.Caption := #$20#$20 + ChangeFileExt(ExtractFileName(Item.Hint),'');
end;

procedure TFormXFade.AudioProcessorGetDataCommon(Sender: TComponent; var Buffer: Pointer;
  var Bytes: Cardinal);
var
      d : P16bitSound;
      x,xm : Int64;
      i : Integer;
      k : Double;

      n_WaveIn : TWaveIn;

begin
      (*(Sender as TAudioProcessor).Input.GetData(Buffer,Bytes);

      case (Sender as TAudioProcessor).Tag of
      1:
        begin
              n_WaveIn := WaveIn1;
        end;
      2:
        begin
              n_WaveIn := WaveIn2;
        end;
      end;


      if Assigned(Buffer) then
      begin
            d := Buffer;

            x := n_WaveIn.Position;
            xm := (n_WaveIn.EndSample-n_WaveIn.StartSample)*4;

            for i := 0 to Bytes div 4 - 1 do
            begin
                  // fade in
                  if (x+i) <= CROSSFADE_SAMPLES then
                  begin
                        k := Max(Min(sin((x+i)/44100*pi/2),1),0);
                        d^.L := Round(d^.L * k);
                        d^.R := Round(d^.R * k);
                  end;

                  // fade out
                  if (xm-(x+i)) <= CROSSFADE_SAMPLES then
                  begin
                        k := Max(Min(cos((x+i)/44100*pi/2),1),0);
                        d^.L := Round(d^.L * k);
                        d^.R := Round(d^.R * k);
                  end;

                  Inc(d);
            end;

      end;
      *)
end;

procedure TFormXFade.LoadProject(FileName:String);
var
      Ini : TMemIniFile;
      Sections : TStringList;
      Section : String;
      fpath : String;
      Data : TSetData;
      i : Integer;
      Item : THTMLItem;
      p_in_s  : String;
      p_out_s : String;
begin
      for i := 0 to AdvPolyList1.ItemCount-1 do TSetData(AdvPolyList1.Tag).Free;
      AdvPolyList1.ClearItems;

      ini := TMemIniFile.Create(FileName,TEncoding.Unicode);
      Sections := TStringList.Create;
      Data := TSetData.Create;
      try
            //ini.Clear;
            ini.ReadSections(Sections);

            for i := 0 to Sections.Count-1 do
            begin
                  //fpath := AdvPolyList1.Items[i].Hint;
                  //Data := Pointer(AdvPolyList1.Items[i].Tag);

                  Section := Sections[i];

                  fpath := ini.ReadString(Section,'FilePath','');
                  Item := OpenFile(fpath);

                  if Assigned(item) then
                  begin
                        TSetData(Item.Tag).StartTime := ini.ReadFloat(Section,'StartTime',-1);
                        TSetData(Item.Tag).EndTime := ini.ReadFloat(Section,'EndTime',-1);

                        p_in_s := Format('%s.%.03d',[TimeToStr(UnixToDateTime(Trunc(TSetData(Item.Tag).StartTime))),Trunc(Frac(TSetData(Item.Tag).StartTime)*1000)]);
                        p_out_s := Format('%s.%.03d',[TimeToStr(UnixToDateTime(Trunc(TSetData(Item.Tag).EndTime))),Trunc(Frac(TSetData(Item.Tag).EndTime)*1000)]);

                        Item.Caption := '<b>'+ChangeFileExt(ExtractFileName(fpath),'') + '</b><br>'+
                        '<font color="#999999"><font size="2">'+p_in_s+'～'+p_out_s+'</font></font>';
                  end;

            end;
      finally
            ini.Free;
            Sections.Free;
            Data.Free;
      end;
end;

procedure TFormXFade.BtnLoadProjectClick(Sender: TObject);
begin
      if OpenDialog2.Execute then
      begin
            LoadProject(OpenDialog2.FileName);
      end;
end;

procedure TFormXFade.HintShowPopup(Sender: TObject);
begin
      HTMLPopup1.Text.Text := '<b>'+(Sender as TWinControl).Hint+'</b>';
      HTMLPopup1.PopupLeft := FormXFade.Left + (Sender as TWinControl).Left;
      HTMLPopup1.PopupTop := (FormXFade.Height - FormXFade.ClientHeight) + FormXFade.Top + (Sender as TWinControl).Top + (Sender as TWinControl).Height;
      HTMLPopup1.Show;
end;

procedure TFormXFade.HintClosePopup(Sender: TObject);
begin
      HTMLPopup1.Hide;
end;

procedure TFormXFade.BtnOpenFilesClick(Sender: TObject);
begin
      if OpenDialog1.Execute then
      begin
            OpenFiles(OpenDialog1.Files);
      end;
end;

procedure TFormXFade.BtnSaveProjectClick(Sender: TObject);
var
      i : Integer;
      fpath : String;
      Data : TSetData;
      Ini : TMemIniFile;
      SHA : TIdhash;
      Section : String;
begin
      if SaveDialog2.Execute then
      begin
            ini := TMemIniFile.Create(SaveDialog2.FileName,TEncoding.Unicode);
            SHA := TIdHashSHA1.Create;
            try
                  ini.Clear;

                  for i := 0 to AdvPolyList1.ItemCount-1 do
                  begin
                        fpath := AdvPolyList1.Items[i].Hint;
                        Data := Pointer(AdvPolyList1.Items[i].Tag);

                        Section := Format('%.04d',[i+1]);
                        //Section := LowerCase(SHA.HashBytesAsHex(TEncoding.ASCII.GetBytes(fpath)));

                        ini.WriteString(Section,'FilePath',fpath);
                        ini.WriteFloat(Section,'StartTime',Data.FStartTime);
                        ini.WriteFloat(Section,'EndTime',Data.FEndTime);
                  end;

                  ini.UpdateFile;
            finally
                  ini.Free;
                  SHA.Free;
            end;
      end;
end;

procedure TFormXFade.CalcCrossFadeConstant(t_sec:Double);
begin
      // const
      CROSSFADE_TIME := t_sec;
      CROSSFADE_BYTES := Trunc(CROSSFADE_TIME * 44100 * 4);
end;

procedure TFormXFade.FormClose(Sender: TObject; var Action: TCloseAction);
var
      i : Integer;
begin
      try
            SaveSettings;
      except
      end;

      for i := AdvPolyList1.ItemCount-1 downto 0 do
      begin
            TSetData(AdvPolyList1.Items[i].Tag).Free;
      end;

      AdvPolyList1.ClearItems;

      // unload wmp media
      WindowsMediaPlayer1.controls.stop;
      WindowsMediaPlayer1.URL := '';
      WindowsMediaPlayer1.currentPlaylist.clear;
end;

procedure TFormXFade.FormCreate(Sender: TObject);
begin
      LoadCompressors;
      LoadSettings;

      AdvPanel1.DoubleBuffered := True;
      CalcCrossFadeConstant(6);

      LinkLabel1.Caption := '<a href="http://limetarte.net">Version '+VersionString+'</a>';
      EnableMainControls(False);
end;

procedure TFormXFade.LoadCompressors();
var
      i : Integer;
      Ini : TmemInifile;
      encoders : TStringList;
      Item : TMenuitem;
begin

      // add compressors
      for i := MenuCompress.Count-1 downto 1 do MenuCompress.Delete(i);
      ini := TMemIniFile.Create('.\addon\encoder.inf');
      encoders := TStringList.Create;
      try
            ini.ReadSectionValues('ENCODERS', encoders);
            for i := 0 to encoders.Count-1 do
            begin
                  if FileExists('.\addon\'+encoders.Names[i]) then
                  begin
                        Item := TMenuItem.Create(AdvPopupMenu1);
                        Item.Caption := encoders.ValueFromIndex[i];
                        Item.Tag := i;
                        Item.RadioItem := True;
                        Item.GroupIndex := 10;
                        item.OnClick := RadioItemClick;
                        MenuCompress.Add(Item);
                  end;
            end;
      finally
            ini.Free;
            encoders.Free;
      end;


end;

procedure TFormXFade.KAddDrop1DropFiles(Sender: TObject; Target: TWinControl;
  DropFiles: TStrings);
begin
      if (DropFiles.Count=1)and(CompareText(ExtractFileExt(DropFiles[0]),'.cfmd')=0) then
      begin
            LoadProject(DropFiles[0]);
      end else
      begin
            OpenFiles(DropFiles);
      end;
end;

procedure TFormXFade.KThdComp1Execute(Sender: TObject);
var
      timeout : Integer;
      endtime : PDouble;
begin
      // waiting play
      timeout := 10{sec} * 10{const};
      while (WindowsMediaPlayer1.playState <> 3) do
      begin
            Dec(timeout);
            if timeout < 0 then exit;
            sleep(100);
      end;

      endtime := Pointer(KthdComp1.Tag);

      // watching stop time
      while (WindowsMediaPlayer1.playState = 3) do
      begin
            if WindowsMediaPlayer1.controls.currentPosition >= endtime^ then
            begin
                  WindowsMediaPlayer1.controls.stop;
                  break;
            end;

            sleep(100);
      end;
end;

procedure TFormXFade.KThdComp1Terminate(Sender: TObject);
begin
      WindowsMediaPlayer1.uiMode := 'full';
      BtnTestPlay.Enabled := True;
end;

procedure TFormXFade.KThdComp2Execute(Sender: TObject);

function CreateFileStream(AFileName:String):TFileStream;
begin
      Result := TFileStream.Create(AFileName,fmCreate);
      Result.Free;
      Result := TFileStream.Create(AFileName,fmOpenReadWrite or fmShareDenyWrite);
end;

var
      //i : Int64;
      pdest : PWideChar;
      dest,sindest : String;
      fpath,ftitle : String;
      o_st,o_ed : Double;
      s_st,s_ed : Int64;
      AudioIn : TAuFileIn;
      i,j,jo,iev,iod : Integer;
      Header : TWaveHeader;
      W,SW : TFileStream;
      wav1,wav2 : TMemoryStream;
      Padding_100ms : TMemoryStream;
      p,pf : P16bitSound;
      pb : Pointer;
      kType : Integer;
      PadTime_Head, PadTime_Tail : Double;
      Makelog : Boolean;
      MakeJson : Boolean;
      BuildHTML : Boolean;
      k : Double;
      logger : TStringList;
      json : TStringList;
      L_time : Double;
      Sample : T16bitSound;
      _encoder : TPushEncoder;
      _forceenc : Boolean;
      _encexe, _encext, _encqua, _encpath : String;
      _html : THTMLBuilder;
      _jsonpath,_jsonrpath : String;
      _basedir : String;
      err : boolean;
      //CROSSFADE_MODES : TCrossFadeMode;
begin
      kType := ComboBox1.ItemIndex;
      Makelog := CheckShiori.Checked;
      BuildHTML := CheckWriteHTML.Checked;
      MakeJson := CheckWriteJson.Checked;

      PadTime_Head := AdvSpinEdit2.FloatValue;
      PadTime_Tail := AdvSpinEdit3.FloatValue;

      CalcCrossFadeConstant(AdvSpinEdit1.FloatValue);
      pdest := Pointer(KThdComp2.Tag);
      dest := pdest; FreeMem(pdest); KThdComp2.Tag := 0;

      // todo : 単品出力を実装する
      // todo : Flash出力を実装する
      // オフメモリは保留
      //CROSSFADE_MODES

      Header.RIFF := 'RIFF';
      Header.FileSize := (* FInput.Size + WaveHeaderOffs - 8 *)0;
      Header.RIFFType := 'WAVE';
      Header.FmtChunkId := 'fmt ';
      Header.FmtChunkSize := 16;
      Header.FormatTag := 1;
      Header.Channels := 2;
      Header.SampleRate := 44100;
      Header.BitsPerSample := 16;
      Header.BlockAlign := (Header.BitsPerSample * Header.Channels) shr 3;
      Header.BytesPerSecond := Header.SampleRate * Header.BlockAlign;
      Header.DataChunkId := 'data';
      Header.DataSize := 0;

      for i := 1 to High(Word) do
      begin
            _basedir := FormatDateTime('yymmdd_'+Format('%.02d',[i]),Now());
      end;

      if (CROSSFADE_MODES = cfmMixed) then
      begin
            W := CreateFileStream(dest);
            _basedir := NewFolderCandidate(ExcludeTrailingPathDelimiter(ExtractFileDir(dest))+'\publish_'+FormatDateTime('yymmdd',Now()));
      end else if (CROSSFADE_MODES = cfmSingle) then
      begin
            W := nil;
            _basedir := NewFolderCandidate(ExcludeTrailingPathDelimiter(dest)+'\publish_'+FormatDateTime('yymmdd',Now()));
      end;

      wav1 := TMemoryStream.Create;
      wav2 := TMemoryStream.Create;
      logger := TStringList.Create;
      json := TStringList.Create;
      Padding_100ms := TMemoryStream.Create;
      _encoder := TPushEncoder.Create('.\addon\','encoder.inf');
      _html := THTMLBuilder.Create('.\template\',_basedir,CROSSFADE_MODES);
      try
            kthdcomp2.Tag := 0;

            // get Encoder settings
            _forceenc := false; _encexe := 'qaac.exe'; _encext := '.m4a'; _encqua := 'MD';
            for i := 0 to MenuCompress.Count-1 do
            begin
                  if (MenuCompress.Items[i].Tag >= 0)and(MenuCompress.Items[i].Checked) then
                  begin
                        _forceenc := true;
                        _encexe := _encoder.GetEncoderExe(MenuCompress.Items[i].Tag);
                        _encext := _encoder.GetEncoderExtension(_encexe);     // exe-extension mapping

                        if MenuCompressLevel.Items[0].Checked then _encqua := 'LO'
                        else if MenuCompressLevel.Items[1].Checked then _encqua := 'MD'
                        else if MenuCompressLevel.Items[2].Checked then _encqua := 'HI';

                        break;
                  end;
            end;

            // force encoding & force json when build htmls
            if (BuildHTML) then
            begin
                  _forceenc := True;
                  MakeJson := True;
            end;

            // Create Directory
            if (_forceenc)or(MakeJson)or(Makelog) then ForceDirectories(_html.BuildDirectory);

            // Make padding source
            for i := 1 to 4410 do
            begin
                  Sample.L := 0;
                  Sample.R := 0;
                  Padding_100ms.Write(Sample,sizeof(Sample));
            end;
            Padding_100ms.Seek(0,0);

            // Initialize Progress notification
            SendMessage(ProgressDlg.Handle,PROGRESS_MSG,0,AdvPolyList1.ItemCount-1);
            SendMessage(ProgressDlg.Handle,ENCODING_MSG,IfThen(_forceenc,1,0),0);

            case CROSSFADE_MODES of
            cfmSingle:
                  begin

                        i := 0;
                        while (i <= AdvPolyList1.ItemCount-1) do
                        begin
                              ftitle := ChangeFileExt(ExtractFileName((AdvPolyList1.Items[i] as TTextItem).Hint),'');
                              fpath := AdvPolyList1.Items[i].Hint;

                              case AnsiIndexText(ExtractFileExt(fpath),['.wav','.mp3','.wma','.aac','.m4a','.mp4']) of
                                    0: AudioIn := WaveIn1;
                                    1: AudioIn := MP3In1;
                                    2: AudioIn := WMIn1;
                                    3,4,5 : AudioIn := AACIn1;
                              end;

                              AudioConverter1.Input := AudioIn;

                              wav1.Clear;

                              // decoding
                              AudioIn.FileName := fpath;

                              o_st := TSetData(AdvPolyList1.Items[i].Tag).StartTime;
                              o_ed := TSetData(AdvPolyList1.Items[i].Tag).EndTime;
                              s_st := Round(o_st * 44100);
                              s_ed := Round(o_ed * 44100);

                              if AudioIn is TWaveIn then
                              begin
                                    (AudioIn as TWaveIn).StartSample := s_st;
                                    (AudioIn as TWaveIn).EndSample := s_ed;
                              end else if AudioIn is TMP3In then
                              begin
                                    (AudioIn as TMP3In).StartSample := s_st;
                                    (AudioIn as TMP3In).EndSample := s_ed;
                              end else if AudioIn is TWMIn then
                              begin
                                    (AudioIn as TWMIn).StartSample := s_st;
                                    (AudioIn as TWMIn).EndSample := s_ed;
                              end else
                              begin
                                    raise Exception.Create('対応していない形式です');
                              end;

                              StreamOut1.Stream := wav1;
                              StreamOut1.Run;

                              repeat
                                    Sleep(100);
                              until (StreamOut1.Status <> tosPlaying);

                              sindest := dest+Format('\Track_%.02d',[i+1])+'.wav';

                              if FileExists(sindest) then
                              begin
                                    case MessageBox(Handle,
                                          Pchar(ExtractFileName(sindest)+'は既に存在します。上書きしますか？'),
                                          Pchar('確認'),
                                          MB_YESNOCANCEL or MB_ICONINFORMATION
                                    ) of
                                          ID_NO :
                                          begin
                                                // progress
                                                SendMessage(ProgressDlg.Handle,PROGRESS_MSG,i,AdvPolyList1.ItemCount-1);
                                                Inc(i);
                                                continue;
                                          end;

                                          ID_CANCEL :
                                          begin
                                                kthdcomp2.Tag := -1;
                                                break;
                                          end;
                                    end;
                              end;

                              W := CreateFileStream(sindest);
                              try
                                    // Write Header
                                    W.Write(Header,sizeof(Header));

                                    // Write Headpadding
                                    for j := 0 to Round(PadTime_Head*10)-1 do
                                    begin
                                          W.Write(PByte(Padding_100ms.Memory)^,Padding_100ms.Size);
                                    end;

                                    // fade in/out
                                    p := wav1.Memory;
                                    //for j := 0 to CROSSFADE_BYTES div 4-1 do
                                    for j := 0 to wav1.Size div 4-1 do
                                    begin
                                          if (j <= CROSSFADE_BYTES div 4) then
                                          begin
                                                jo := j;
                                                case kType of
                                                 0 : k := jo/(CROSSFADE_BYTES div 4-1);
                                                 1 : k := Max(Min(sqr(sin(jo/(CROSSFADE_BYTES div 4-1)*pi/2)),1),0);
                                                end;
                                          end else if (j >= (wav1.Size-CROSSFADE_BYTES) div 4) then
                                          begin
                                                jo := j - (wav1.Size-CROSSFADE_BYTES) div 4;
                                                case kType of
                                                 0 : k := (CROSSFADE_BYTES div 4-1-jo)/(CROSSFADE_BYTES div 4-1);
                                                 1 : k := Max(Min(sqr(cos(jo/(CROSSFADE_BYTES div 4-1)*pi/2)),1),0);
                                                end;
                                          end else
                                          begin
                                                k := 1;
                                          end;

                                          p^.L := Round(p^.L * k);
                                          p^.R := Round(p^.R * k);
                                          Inc(p);
                                    end;

                                    p := wav1.Memory;
                                    W.Write(PByte(p)^,wav1.Size);

                                    // Write Tail-padding
                                    for j := 0 to Round(PadTime_Tail*10)-1 do
                                    begin
                                          W.Write(PByte(Padding_100ms.Memory)^,Padding_100ms.Size);
                                    end;

                                    Header.FileSize := W.Size + 44 - 8;
                                    Header.DataSize := W.Size;

                                    W.Seek(0,0);
                                    W.Write(Header,sizeof(Header));

                                    if (_forceenc) then
                                    begin
                                          W.Free; W := nil;
                                          _encpath := _html.BuildDirectory+ExtractFileName(ChangeFileExt(sindest,_encext));
                                          _encoder.Add(sindest,_encpath,_encexe,_encqua,'TITLE="'+ftitle+'"');
                                    end;

                                    if (BuildHTML) then _html.Write(ftitle, _encpath);

                              finally
                                    FreeAndNil(W);
                              end;

                              // progress
                              SendMessage(ProgressDlg.Handle,PROGRESS_MSG,i,AdvPolyList1.ItemCount-1);

                              Inc(i);
                        end;

                  end;

            cfmMixed:
                  begin
                        json.Add('[');

                        // Write Header
                        W.Write(Header,sizeof(Header));

                        // Write Headpadding
                        for i := 0 to Round(PadTime_Head*10)-1 do
                        begin
                              W.Write(PByte(Padding_100ms.Memory)^,Padding_100ms.Size);
                        end;

                        i := 0;
                        while (i <= AdvPolyList1.ItemCount-1) do
                        begin
                              fpath := AdvPolyList1.Items[i].Hint;

                              case AnsiIndexText(ExtractFileExt(fpath),['.wav','.mp3','.wma','.aac','.m4a','.mp4']) of
                                    0: AudioIn := WaveIn1;
                                    1: AudioIn := MP3In1;
                                    2: AudioIn := WMIn1;
                                    3,4,5 : AudioIn := AACIn1;
                              end;

                              AudioConverter1.Input := AudioIn;

                              wav1.Clear;

                              // decoding
                              AudioIn.FileName := fpath;

                              o_st := TSetData(AdvPolyList1.Items[i].Tag).StartTime;
                              o_ed := TSetData(AdvPolyList1.Items[i].Tag).EndTime;
                              s_st := Round(o_st * 44100);
                              s_ed := Round(o_ed * 44100);

                              if AudioIn is TWaveIn then
                              begin
                                    (AudioIn as TWaveIn).StartSample := s_st;
                                    (AudioIn as TWaveIn).EndSample := s_ed;
                              end else if AudioIn is TMP3In then
                              begin
                                    (AudioIn as TMP3In).StartSample := s_st;
                                    (AudioIn as TMP3In).EndSample := s_ed;
                              end else if AudioIn is TWMIn then
                              begin
                                    (AudioIn as TWMIn).StartSample := s_st;
                                    (AudioIn as TWMIn).EndSample := s_ed;
                              end else
                              begin
                                    raise Exception.Create('対応していない形式です');
                              end;

                              StreamOut1.Stream := wav1;
                              StreamOut1.Run;

                              repeat
                                    Sleep(100);
                              until (StreamOut1.Status <> tosPlaying);

                              wav1.Seek(0,0);

                              // fade in
                              p := wav1.Memory;
                              for j := 0 to CROSSFADE_BYTES div 4-1 do
                              begin
                                    case kType of
                                     0 : k := j/(CROSSFADE_BYTES div 4-1);
                                     1 : k := Max(Min(sqr(sin(j/(CROSSFADE_BYTES div 4-1)*pi/2)),1),0);
                                    end;
                                    p^.L := Round(p^.L * k);
                                    p^.R := Round(p^.R * k);
                                    Inc(p);
                              end;

                              if Makelog then
                              begin
                                    L_time := ((W.Size-sizeof(Header)) / (44100*4)) + CROSSFADE_TIME / 2;
                                    logger.Add(Format('[%s.%.03d] : %s <%s.%.03d～%s.%.03d>',[
                                          TimeToStr(UnixToDateTime(Trunc(L_time))),Trunc(Frac(L_time)*1000),
                                          ExtractFileName(fpath),
                                          TimeToStr(UnixToDateTime(Trunc(o_st))),Trunc(Frac(o_st)*1000),
                                          TimeToStr(UnixToDateTime(Trunc(o_ed))),Trunc(Frac(o_ed)*1000)
                                    ]));
                              end;

                              if MakeJson then
                              begin
                                    json.Add('  {');
                                    json.Add('    '+Format('"track":"%s",',[ChangeFileExt(ExtractFileName(fpath),'').Replace('"','\"')]));
                                    json.Add('    '+Format('"absbegin":"%.02f",',[(W.Size-sizeof(Header)) / (44100*4)]));
                                    json.Add('    '+Format('"absduration":"%.02f",',[wav1.Size/(44100*4)]));
                                    json.Add('    '+Format('"srcbegin":"%.02f",',[o_st]));
                                    json.Add('    '+Format('"srcduration":"%.02f"',[o_ed-o_st]));
                                    json.Add('  },');
                              end;

                              // mixing
                              p := wav1.Memory;
                              pf := wav2.Memory;
                              if (wav2.Size > 0)and(wav2.Memory <> nil) then
                              begin
                                    for j := 0 to wav2.Size div 4 - 1 do
                                    begin
                                          p^.L := p^.L + pf^.L;
                                          p^.R := p^.R + pf^.R;
                                          Inc(p); Inc(pf);
                                    end;
                              end;

                              // fade out
                              p := wav1.Memory; Inc(p, wav1.Size div 4); Dec(p,CROSSFADE_BYTES div 4); pb := p;
                              for j := 0 to CROSSFADE_BYTES div 4-1 do
                              begin
                                    case kType of
                                     0 : k := (CROSSFADE_BYTES div 4-1-j)/(CROSSFADE_BYTES div 4-1);
                                     1 : k := Max(Min(sqr(cos(j/(CROSSFADE_BYTES div 4-1)*pi/2)),1),0);
                                    end;

                                    p^.L := Round(p^.L * k);
                                    p^.R := Round(p^.R * k);
                                    Inc(p);
                              end;

                              Inc(i);

                              // write to file
                              p := wav1.Memory;
                              W.Write(PByte(p)^,wav1.Size);
                              W.Seek(-CROSSFADE_BYTES,soFromCurrent);

                              wav2.Clear;
                              wav2.Write(PByte(pb)^,CROSSFADE_BYTES);
                              wav2.Seek(0,0);

                              // progress
                              SendMessage(ProgressDlg.Handle,PROGRESS_MSG,i,AdvPolyList1.ItemCount-1);
                        end;

                        W.Seek(0,soFromEnd);

                        // Write Tail-padding
                        for i := 0 to Round(PadTime_Tail*10)-1 do
                        begin
                              W.Write(PByte(Padding_100ms.Memory)^,Padding_100ms.Size);
                        end;

                        Header.FileSize := W.Size + 44 - 8;
                        Header.DataSize := W.Size;

                        W.Seek(0,0);
                        W.Write(Header,sizeof(Header));

                        // post process
                        _jsonpath := _html.BuildDirectory + ExtractFileName(ChangeFileExt(dest,'.json'));
                        _jsonrpath := ExtractRelativePath(_html.BuildDirectory,_jsonpath).Replace('\','/');

                        if (_forceenc) then
                        begin
                              W.Free; W := nil;
                              _encpath := _html.BuildDirectory+ExtractFileName(ChangeFileExt(dest,_encext));
                              _encoder.Add(dest,_encpath,_encexe,_encqua,'TITLE='+ExtractFileName(ChangeFileExt(dest,'')).QuotedString('"'));
                        end;

                        if (BuildHTML) then _html.Write(ftitle, _encpath, 'OFFSET='+AdvSpinEdit1.FloatValue.ToString()+#13#10+'JSON='+_jsonrpath);

                        if Makelog then logger.SaveToFile(_html.BuildDirectory+ChangeFileExt(ExtractFileName(dest),'.txt'), TEncoding.UTF8);

                        json.Delete(json.Count-1);
                        json.Add(' }');
                        json.Add(']');

                        if MakeJson then json.SaveToFile(_jsonpath,TEncoding.UTF8);
                    end;
            end;

            // wait encoding.
            while (_encoder.QueueCount > 0) do
            begin
                  Sleep(100);
            end;

            SendMessage(ProgressDlg.Handle,ENCODING_MSG,0,0);

            if (BuildHTML) then _html.Publish('soundsheet.html');

      finally
            _encoder.Terminate;
            FreeAndNil(W);
            FreeAndNil(_html);
            wav1.Free;
            wav2.Free;
            logger.Free;
            json.Free;
            Padding_100ms.Free;
      end;

end;

procedure TFormXFade.KThdComp2Terminate(Sender: TObject);
begin
      if KthdComp2.Tag = 0 then
      begin
            MessageBox(ProgressDlg.handle,'完了しました。','Complete',MB_OK or MB_ICONINFORMATION);
      end else
      begin
            MessageBox(ProgressDlg.handle,'途中終了しました。','Complete',MB_OK or MB_ICONWARNING);
      end;

      ProgressDlg.Close;
      ProgressDlg.Release;
end;

procedure TFormXFade.TimeCodeClick(Sender: TObject);
var
      TimeCode : Double;
begin
      TimeCode := PDouble((Sender as TLabel).Tag)^;
      if TimeCode >= 0 then
      begin
            WindowsMediaPlayer1.controls.currentPosition := TimeCode;
            if (WindowsMediaPlayer1.playState <> 3) then
            begin
                  WindowsMediaPlayer1.controls.play;
            end;
      end;
end;

procedure TFormXFade.TimeCodeLabelMouseEnter(Sender: TObject);
begin
      with (Sender as TLabel) do
      begin
            Font.Color := clHighLight;
            Font.Style := Font.Style + [fsUnderline];
      end;
end;

procedure TFormXFade.TimeCodeLabelMouseLeave(Sender: TObject);
begin
      with (Sender as TLabel) do
      begin
            Font.Color := clWindowText;
            Font.Style := Font.Style - [fsUnderline];
      end;
end;

procedure TFormXFade.LinkLabel1LinkClick(Sender: TObject; const Link: string;
  LinkType: TSysLinkType);
begin
      if LinkType = sltURL then
      begin
            ShellExecute(Handle,'open',PChar(Link),nil,nil,SW_SHOW);
      end;
end;

initialization
      CoInitializeEx(nil,COINIT_MULTITHREADED);
      OleInitialize(nil);

finalization
      CoUnInitialize;
      OleUnInitialize;
end.
