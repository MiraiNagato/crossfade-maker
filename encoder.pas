unit encoder;

interface

uses
  Windows, Sysutils, Classes, System.Generics.Collections, IniFiles;

type
  TQueueInfo = record
      Source : String;
      Dest : String;
      Encoder : String;
      Quality : String;
      GenericFlag : Array [0..4] of Boolean;
      Variables : String;
  end;

  TPushEncoder = class(TThread)
  private
      FQueue : TList<TQueueInfo>;
      FPeeked : TQueueInfo;
      FDefines : TMemIniFile;
      FDirBase : String;
      FDummyCount : Integer;
      procedure Peek;
      function ReplaceParam(Param:String; VarSets:TStrings):String;
      function GetQueueCount:Integer;
  protected
      procedure Execute; override;
  public
      constructor Create(DirBase:String; DefineFile:String);
      destructor Destroy; override;
      procedure Add(Source,Dest,Encoder,Quality:String; Variables:String = '');
      procedure GetEncoders(AEncoders:TStrings; Section:String = 'ENCODERS');
      function GetEncoderExe(index:Integer):String;
      function GetEncoderExtension(Exe:String):String;

      function GetParam(Encoder:String; Quality:String):String;
      property QueueCount:Integer read GetQueueCount;
  end;

implementation

procedure TPushEncoder.Peek;
begin
      if (FQueue.Count > 0) then
      begin
            FPeeked := FQueue[0];
            FQueue.Delete(0);
            FDummyCount := 1;
      end;
end;

function TPushEncoder.GetQueueCount:Integer;
begin
      Result := FQueue.Count + FDummyCount;
end;

procedure TPushEncoder.GetEncoders(AEncoders:TStrings; Section:String = 'ENCODERS');
begin
      FDefines.ReadSectionValues(Section,AEncoders);
end;

function TPushEncoder.GetEncoderExe(index:Integer):String;
var
      Encs : TStringList;
begin
      Encs := TStringList.Create;
      try
            GetEncoders(Encs);
            Result := Encs.Names[index];
      finally
            Encs.Free;
      end;
end;

function TPushEncoder.GetEncoderExtension(Exe:String):String;
var
      Encs : TStringList;
begin
      Encs := TStringList.Create;
      try
            GetEncoders(Encs,'EXTENSION');
            Result := Encs.Values[Exe];
      finally
            Encs.Free;
      end;

end;

function TPushEncoder.GetParam(Encoder:String; Quality:String):String;
var
      Data : TStringList;
begin
      Data := TStringList.Create;
      try
            FDefines.ReadSectionValues('PARAMETER-'+Quality,Data);
            Result := Data.Values[Encoder];
      finally
            Data.Free;
      end;
end;

function TPushEncoder.ReplaceParam(Param:String; VarSets:TStrings):String;
var
      i : Integer;
      Key : String;
      kf : Boolean;
begin
      Result := '';
      kf := False;

      for i := 1 to Length(Param) do
      begin
            if (not kf) then
            begin
                  if Param[i] = '<' then
                  begin
                        key := '';
                        kf := True;
                        continue;
                  end;

                  Result := Result + Param[i];
            end else
            begin
                  if Param[i] = '>' then
                  begin
                        Result := Result + VarSets.Values[Key];
                        key := '';
                        kf := False;
                        continue;
                  end;

                  Key := Key + Param[i];
            end;
      end;
end;

procedure TPushEncoder.Execute;
var
      INF : TQueueInfo;
      Encoders : TStringList;
      Vars : TStringList;
      Param : String;
      StartupInfo: TStartupInfo;
      ProcessInfo: TProcessInformation;
begin
      Encoders := TStringList.Create;
      Vars := TStringList.Create;
      try
            GetEncoders(Encoders);

            while (not Self.Terminated) do
            begin
                  FPeeked.GenericFlag[0] := False;
                  Synchronize(Peek);

                  if (FPeeked.GenericFlag[0]) then
                  begin
                        Param := FDirBase + FPeeked.Encoder + ' ' + GetParam(FPeeked.Encoder,FPeeked.Quality);

                        Vars.Clear;
                        Vars.Text := FPeeked.Variables;
                        Vars.Values['INPUT'] := FPeeked.Source.QuotedString('"');
                        Vars.Values['OUTPUT'] := FPeeked.Dest.QuotedString('"');
                        //Vars.Values['TITLE'] := ChangeFileExt(ExtractFileName(FPeeked.Source),'').QuotedString('"');

                        Param := ReplaceParam(Param,Vars);

                        FillChar(StartupInfo, SizeOf(StartupInfo), #0);
                        StartupInfo.cb          := SizeOf(StartupInfo);
                        StartupInfo.dwFlags     := STARTF_USESHOWWINDOW;
                        StartupInfo.wShowWindow := SW_HIDE;

                        // RUN PROCESS
                        if CreateProcess(nil,
                          PChar(Param),      // pointer to command line string
                          nil,                  // pointer to process security attributes
                          nil,                  // pointer to thread security attributes
                          False,                // handle inheritance flag
                          CREATE_NEW_CONSOLE or // creation flags
                          NORMAL_PRIORITY_CLASS,
                          nil,                  //pointer to new environment block
                          nil,                  // pointer to current directory name
                          StartupInfo,          // pointer to STARTUPINFO
                          ProcessInfo)          // pointer to PROCESS_INF
                        then
                        begin
                              while WaitForSingleObject(ProcessInfo.hProcess,100) = WAIT_TIMEOUT do
                              begin
                                    if (Self.Terminated) then
                                    begin
                                          TerminateProcess(ProcessInfo.hProcess,0);
                                          break;
                                    end;
                              end;
                              //GetExitCodeProcess(ProcessInfo.hProcess, Result);
                              CloseHandle(ProcessInfo.hProcess);
                              CloseHandle(ProcessInfo.hThread);
                        end;

                        FDummyCount := 0;
                  end;

                  Sleep(100);
            end;
      finally
            Encoders.Free;
            Vars.Free;
      end;
end;

procedure TPushEncoder.Add(Source,Dest,Encoder,Quality:String; Variables:String = '');
var
      INF : TQueueInfo;
begin
      INF.GenericFlag[0] := True;
      INF.Source := Source;
      INF.Dest := Dest;
      INF.Encoder := Encoder;
      INF.Quality := Quality;
      INF.Variables := Variables;
      FQueue.Add(INF);
end;

constructor TPushEncoder.Create(DirBase:String; DefineFile:String);
begin
      inherited Create(False);
      FreeOnTerminate := True;
      FQueue := TList<TQueueInfo>.Create;
      FDirBase := IncludeTrailingPathDelimiter(DirBase);
      FDefines := TMemIniFile.Create(FDirBase+DefineFile);
end;

destructor TPushEncoder.Destroy;
begin
      FQueue.Free;
      FDefines.Free;
end;

end.
