unit common;

interface

uses
  Sysutils;

type
  TCrossFadeMode = (cfmMixed,cfmSingle);

  P16bitSound = ^T16bitSound;
  T16bitSound = record
      L : SmallInt;
      R : SmallInt;
  end;

function NewFolderCandidate(Candidate:String):String;

implementation

function NewFolderCandidate(Candidate:String):String;
var
      i : Integer;
      _basedir : String;
begin
      for i := 1 to High(Word) do
      begin
            _basedir := Candidate + '_' + Format('%.02d',[i]);
            if not DirectoryExists(_basedir) then
            begin
                  Result := _basedir;
                  break;
            end;
      end;
end;

end.
