﻿unit Unit3;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls, AdvSmoothProgressBar,
  Vcl.ExtCtrls;

const
  PROGRESS_MSG = WM_APP + $1;
  ENCODING_MSG = WM_APP + $2;

type

  TProgressDlg = class(TForm)
    ProgressBar: TAdvSmoothProgressBar;
    Label1: TLabel;
    Label2: TLabel;
    Timer1: TTimer;
    procedure Timer1Timer(Sender: TObject);
  private
    { Private 宣言 }
  public
    { Public 宣言 }
    procedure Receive(var Msg:TMessage); message PROGRESS_MSG;
    procedure EncReceive(var Msg:TMessage); message ENCODING_MSG;
  end;

var
  ProgressDlg: TProgressDlg;

implementation

{$R *.dfm}

procedure TProgressDlg.Receive(var Msg:TMessage);
begin
      ProgressBar.Maximum := Msg.LParam;
      ProgressBar.Position := Msg.WParam;
end;

procedure TProgressDlg.Timer1Timer(Sender: TObject);
begin
      if (Timer1.Enabled) then Label2.Visible := not Label2.Visible;
end;

procedure TProgressDlg.EncReceive(var Msg:TMessage);
begin
      if Msg.WParam = 0 then
      begin
            Timer1.Enabled := False;
            Label2.Visible := False;
      end else
      begin
            Label2.Visible := True;
            Timer1.Enabled := True;
      end;
end;

end.
